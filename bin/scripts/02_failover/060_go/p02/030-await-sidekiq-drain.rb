#!/usr/bin/gitlab-rails runner

$purge_allowed = Set[
  "pages_domain_verification",
  "geo:geo_repository_verification_primary_shard"
]

# Staging: Some jobs (e.g., "file_download_dispatch_worker") may refuse to exit. They can be safely ignored.
if ENV["FAILOVER_ENVIRONMENT"] == "stg" || `hostname -f` == "deploy.stg.gitlab.com" # Hack to get around inability to pass envvars
  $purge_allowed << "file_download_dispatch_worker"
  $purge_allowed << "emails_on_push"
  $purge_allowed << "mailers"
  $purge_allowed << "background_migration"
end

$dry_run = false

def queue_can_be_purged(queue_name)
  # Make sure that the geo crons are not included in this list...
  $purge_allowed.include?(queue_name)
end

def cronjob_can_be_disabled(cron_name)
  # Okay to disable this Geo job, but...

  !::Gitlab::Geo::CronManager::GEO_JOBS.include?(cron_name)
end

def handle_named_set(title, named_set)
  background_migrations = {}
  queue_sizes = named_set.each_with_object({}) do |job, hash|
    hash[job.queue] = (hash[job.queue] || 0) + 1
    if job.queue == 'background_migration'
      background_migrations[job.args[0]] = (background_migrations[job.args[0]] || 0) + 1
    end
  end

  if !queue_sizes.empty?
    puts "#{title}:"

    queue_sizes.each do |k,v|
      status = queue_can_be_purged(k) ? " (purged)" : ""
      puts "  #{k}: #{v}#{status}"
      if k == 'background_migration' && !background_migrations.empty?
        background_migrations.each { |klass, count| puts "    #{klass}: #{count}"}
      end
    end
  end

  ## Remove jobs that that we do not need to retry.
  unless $dry_run
    named_set.each do |job|
      job.delete if queue_can_be_purged(job.queue)
    end
  end
end

begin
  loop do
    puts "----------------------------------------------------------------------------------------"
    if $dry_run
      puts "NOTE: This script is in dry run mode and will not purge any queues"
    else
      puts "WARNING: This script is in terminator mode. It will hunt down unwanted jobs and kill them off"
    end
    pending_queues = Sidekiq::Queue.all.select { |q| q.size > 0 }

    unless pending_queues.empty?
      puts "Queues:"
      pending_queues.each do |q|
        if queue_can_be_purged(q.name)
          q.clear unless $dry_run
          puts "  #{q.name}: #{q.size} (purged)"
        else
          puts "  #{q.name}: #{q.size}"
        end
      end
    end

    handle_named_set("Retries", Sidekiq::RetrySet.new)
    handle_named_set("Scheduled", Sidekiq::ScheduledSet.new)
    # Ignore the dead queue....

    ps = Sidekiq::ProcessSet.new
    total_busy = ps.each_with_object({}) do |process, hash|
      # Hack the hostname to figure out the sidekiq cluster name
      m = /sidekiq-(\w+)-\d+|(worker)\d+.cluster.gitlab.com/.match(process["hostname"])
      group = m && (m[1] || m[2]) || process["hostname"]

      hash[group] = (hash[group] || 0) + process["busy"]
    end

    unless total_busy.empty?
      puts "Busy jobs per worker type:"
      total_busy.each { |k, v| puts "  #{k}: #{v}" }
    end

    crons = Sidekiq::Cron::Job.all
    total_enabled_sidekiq_crons = crons.select { |cron| cron.status == "enabled" }.count

    ## Disable most cronjobs
    unless $dry_run
      crons.select { |cron| cron.status == "enabled" && cronjob_can_be_disabled(cron.name) }.each(&:disable!)
    end

    puts "Cron: #{total_enabled_sidekiq_crons} enabled out of #{crons.length} total"

    stats = Sidekiq::Stats.new
    total_enqueued = stats.enqueued
    total_retries = Sidekiq::RetrySet.new.size
    total_scheduled = Sidekiq::ScheduledSet.new.size
    puts "\nTotal Enqueued: #{total_enqueued} | Total Retries: #{total_retries} | Total Scheduled: #{total_scheduled}\n"

    if total_enqueued.zero? && total_retries.zero? && total_scheduled.zero?
      puts "--> Status: PROCEED.\n--> Continue with the migration.\n--> So long. And good luck (keep this script running)!"
    else
      puts "--> Status: WAIT"
    end

    puts "\n\n"

    sleep 1
  end
rescue Interrupt => e
end
